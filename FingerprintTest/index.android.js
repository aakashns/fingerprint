import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableNativeFeedback,
  ToastAndroid,
  DeviceEventEmitter,
  NativeModules,
  BackAndroid,
  Navigator,
} from 'react-native';
import AuthScreen from './app/screens/AuthScreen';

const { FingerprintAuthentication } = NativeModules;
const Touchable = TouchableNativeFeedback;
const KEY = 'example_key';

class FingerprintTest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      waiting: false,
    };
  }

  componentDidMount() {
    const authListener = DeviceEventEmitter.addListener('fingerprintAuthenticationResult', (e) => {
      console.log(e);
      if (e.authenticated) {
        ToastAndroid.show("Authentication successful!", ToastAndroid.SHORT)
      } else {
        ToastAndroid.show("Authentication failed! " + e.errorMessage + " " + e.errorCode, ToastAndroid.SHORT)
      }
      this.setState({ waiting: false });
      FingerprintAuthentication.stopListeningForAuth();
    });
    console.log(authListener);
  }

  init() {
    console.log('Initializing the finger print');
    FingerprintAuthentication.init(KEY)
      .then(() => {
        console.log("Initialization successful");
        ToastAndroid.show("Initialization successful", ToastAndroid.SHORT);
      })
      .catch(e => {
        ToastAndroid.show("Initialization failed! " + e.message + " (" + e.code + ")", ToastAndroid.SHORT);
        console.error(e, e.code);
      });
  };

  authenticate() {
    console.log('Initializing authentication');
    this.setState({ waiting: true });
    FingerprintAuthentication.listenForAuth();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to Fingerprint Authentication demo!
        </Text>
        <Text style={styles.instructions}>
          To get started, click on 'Initialize', then click on 'Authenticate'.
        </Text>
        <View style={{padding: 16, alignSelf: 'stretch'}}>
          <Button
            label="Initialize"
            onPress={() => this.init()}
          />
          <View style={{height: 16}} />
          <Button
            label="Authenticate"
            onPress={() => this.authenticate()}
          />
        </View>
        {this.state.waiting &&
          <Text style={styles.instructions}>
            Place your finger on the sensor..
          </Text>
        }

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

class App extends Component {
  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      const { navigator } = this.refs;
      if (navigator && navigator.getCurrentRoutes().length > 1) {
        navigator.pop();
        return true;
      }
      return false;
    });
  }

  renderScene(route, navigator) {
      const Component = route.component;
      return (
        <Component
          navigator={navigator}
          route={route}
          {...route.passProps}
        />
      );
  }

  render() {
    return (
      <Navigator
        ref='navigator'
        style={{flex: 1}}
        initialRoute={{
          component: AuthScreen,
        }}
        renderScene={this.renderScene}
      />
    )
  }
}


AppRegistry.registerComponent('FingerprintTest', () => App);


// Button component for Android
const BUTTON_COLOR = '#3F51B5';
const Button = ({
  onPress,
  label,
}) => (
  <TouchableNativeFeedback onPress={onPress}>
    <View style={buttonStyles.button}>
      <Text style={buttonStyles.buttonText}>
        {label.toUpperCase()}
      </Text>
    </View>
  </TouchableNativeFeedback>
);

const buttonStyles = StyleSheet.create({
  button: {
    height: 36,
    backgroundColor: BUTTON_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
    elevation: 2,
  },
  buttonText: {
    fontSize: 14,
    fontFamily: 'sans-serif-medium',
    color: 'white',
  }
});
