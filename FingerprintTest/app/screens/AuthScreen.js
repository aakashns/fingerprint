import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ToolbarAndroid,
  ToastAndroid,
  Image,
  Picker,
  NativeModules,
  DeviceEventEmitter,
} from 'react-native';
import Header from '../components/Header';
import ListScreen from './ListScreen';

const { FingerprintAuthentication } = NativeModules;
const KEY = 'example_key';

class AuthScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: '1'
    };
  }

  componentDidMount() {
    DeviceEventEmitter.addListener('fingerprintAuthenticationResult', (e) => {
      if (e.authenticated) {
        ToastAndroid.show("Authentication successful!", ToastAndroid.SHORT);
        const { navigator } = this.props;
        navigator.push({
          component: ListScreen,
        });
      } else {
        console.log(e);
        if (e.errorCode != 'NOT_INITIALIZED') {
            ToastAndroid.show("Error: " + e.errorMessage, ToastAndroid.SHORT);
        }
      }
    });

    FingerprintAuthentication.init(KEY)
      .catch(err => {
        console.error(err);
        ToastAndroid.show("Error: " + err.message, ToastAndroid.SHORT);
      })
      .then(() => {
        FingerprintAuthentication.listenForAuth();
      });
  }

  componentWillUnmount() {
      FingerprintAuthentication.stopListeningForAuth();
  }

  render() {
    return (
      <View style={styles.container}>
        <Header />
        <View style={styles.body}>
          <Text style={styles.text}>
            Select an account from the list below
          </Text>
          <Picker
            style={styles.picker}
            mode='dropdown'
            selectedValue={this.state.selected}
            onValueChange={selected => this.setState({selected})}>
            <Picker.Item label="XXX-XXX-XXX-4774" value="1" />
            <Picker.Item label="XXX-XXX-XXX-4780" value="2" />
            <Picker.Item label="XXX-XXX-XXX-4790" value="3" />
            <Picker.Item label="XXX-XXX-XXX-4795" value="4" />
            <Picker.Item label="XXX-XXX-XXX-4800" value="5" />
            <Picker.Item label="XXX-XXX-XXX-4814" value="6" />
          </Picker>
          <Text style={styles.text}>
            Touch sensor to view transactions
          </Text>
          <Image
            style={styles.image}
            source={require('../img/fingerprint.jpg')}
          />
        </View>
      </View>
    );
  }
}

export default AuthScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  body: {
    flex: 1,
    padding: 16,
    alignItems: 'center',
  },
  image: {
    resizeMode: 'contain',
    height: 240,
    marginTop: 16,
  },
  picker: {
    width: 200,
    marginTop: 16,
    marginBottom: 16,
  },
  text: {
    color: 'black',
  },
});
