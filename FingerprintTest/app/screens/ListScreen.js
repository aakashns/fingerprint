import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  ListView,
} from 'react-native';
import Header from '../components/Header';
import Colors from '../util/Colors';

const DUMMY_DATA = [
  'Halley Ohagan',
  'Denis Widener',
  'Jeanett Linz',
  'Remedios Upshur',
  'Marion Jost',
  'Ashleigh Poythress',  
  'Keren Losh',  
  'Cyndy Goolsby',  
  'Arron Cirillo',  
  'Jann Herndon',  
  'Elma Anstine',  
  'Elisha Pearl',  
  'Marni Jonason',  
  'Breana Stidham',  
  'Synthia Dahmen',  
  'Mark Suber',  
  'Concha Bannister',  
  'Bernarda Crisp',  
  'Bill Manzanares',  
  'Tiara Hodson',  
];

export default class ListScreen extends Component {
  constructor(props) {
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      loading: true,
      dataSource: ds.cloneWithRows(DUMMY_DATA),
    };
  }

  componentDidMount() {
    setTimeout(() => this.setState({loading: false}), 1500);
  }

  renderRow(text) {
    return (
      <View style={styles.listItem}>
        <Text style={styles.text}>{text}</Text>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Header />
        {this.state.loading ?
            <View style={styles.progressWrapper}>
              <ActivityIndicator
                size='large'
                color={Colors.PRIMARY}
              />
            </View> :

            <ListView
              dataSource={this.state.dataSource}
              renderRow={this.renderRow}
            />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  progressWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  listItem: {
    borderBottomWidth: 1,
    borderColor: '#CCCCCC',
    paddingTop: 16,
    paddingBottom: 16,
    marginLeft: 16,
    marginRight: 16,
  },
  text: {
    color: 'black',
  }
});
