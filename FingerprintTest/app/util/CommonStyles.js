import Colors from './Colors';

const CommonStyles = {
  toolbar: {
    height: 56,
    backgroundColor: Colors.PRIMARY,
  },
  title: {
    fontSize: 24,
    color: Colors.PRIMARY_TEXT,
  },
  titleWrapper: {
    height: 64,
    paddingLeft: 16,
    justifyContent: 'center',
    marginTop: -8,
  },
  label: {
    fontSize: 12,
    marginLeft: 4,
    color: Colors.SECONDARY_TEXT,
  },
  progress: {
    marginTop: -7,
  },
}

export default CommonStyles;
