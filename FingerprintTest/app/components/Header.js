import React from 'react';
import {
  View,
  ToolbarAndroid,
  ProgressBarAndroid,
  StatusBar,
} from 'react-native';
import CommonStyles from '../util/CommonStyles';
import Colors from '../util/Colors';
import Constants from '../util/Constants';

const Progress = ProgressBarAndroid;

const Header = ({ loading }) => (
  <View>
    <StatusBar backgroundColor={Colors.PRIMARY_DARK} />
    <ToolbarAndroid
      style={CommonStyles.toolbar}
      title={Constants.APP_NAME}
      titleColor={Colors.LIGHT_TEXT}
    />
    {/*<Progress
      color={Colors.ACCENT}
      indeterminate
      styleAttr="Horizontal"
      style={CommonStyles.progress}
      opacity={loading ? 1 : 0}
    />*/}
  </View>
);

export default Header;
