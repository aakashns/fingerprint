package com.reactutils.modules;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.v4.app.ActivityCompat;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class FingerprintAuthenticationModule extends ReactContextBaseJavaModule {
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private FingerprintManager.CryptoObject cryptoObject;
    private boolean initialized = false;
    private CancellationSignal cancellationSignal;

    // Errors thrown by .init()
    private static final String KEY_NOT_PROVIDED = "KEY_NOT_PROVIDED";
    private static final String ANDROID_VERSION_TOO_LOW = "ANDROID_VERSION_TOO_LOW";
    private static final String LOCK_SCREEN_NOT_ENABLED = "LOCK_SCREEN_NOT_ENABLED";
    private static final String FINGERPRINT_PERMISSION_NOT_GRANTED = "FINGERPRINT_PERMISSION_NOT_GRANTED";
    private static final String NO_FINGERS_REGISTERED = "NO_FINGERS_REGISTERED";

    // Errors thrown by .startAuth()
    private static final String NOT_INITIALIZED = "NOT_INITIALIZED";
    private static final String AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED";
    private static final String AUTHENTICATION_ERROR = "AUTHENTICATION_ERROR";
    private static final String AUTHENTICATION_HELP = "AUTHENTICATION_HELP";
    private static final String REQUEST_CANCELLED = "REQUEST_CANCELLED";

    public FingerprintAuthenticationModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "FingerprintAuthentication";
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    /** Javascript method to initialize the module with a given key **/
    @ReactMethod
    public void init(String keyName, Promise promise) {
        if (keyName == null) {
            promise.reject(KEY_NOT_PROVIDED, "No key provided.");
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            promise.reject(ANDROID_VERSION_TOO_LOW, "Android version M or higher is required.");
        } else {
            keyguardManager = (KeyguardManager) getReactApplicationContext()
                    .getSystemService(Context.KEYGUARD_SERVICE);
            fingerprintManager = (FingerprintManager) getReactApplicationContext()
                    .getSystemService(Context.FINGERPRINT_SERVICE);

            if (!keyguardManager.isKeyguardSecure()) {
                promise.reject(LOCK_SCREEN_NOT_ENABLED, "Lock screen not enabled");
                return;
            }

            if (ActivityCompat.checkSelfPermission(getReactApplicationContext(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                promise.reject(FINGERPRINT_PERMISSION_NOT_GRANTED, "Fingerprint auth permission is not granted.");
                return;
            }

            if (!fingerprintManager.hasEnrolledFingerprints()) {
                promise.reject(NO_FINGERS_REGISTERED, "Please register at least one fingerprint");
                return;
            }

            try {
                generateKey(keyName);
                cipherInit(keyName);
                cryptoObject = new FingerprintManager.CryptoObject(cipher);
                initialized = true;
                promise.resolve(null);
            } catch (Exception e) {
                promise.reject(e);
            }
        }
    }

    private void sendResult(boolean authenticated, String errorCode, String errorMessage) {
        sendEvent(getReactApplicationContext(), "fingerprintAuthenticationResult",
                makeResult(authenticated, errorCode, errorMessage));
    }

    /** Javascript method to initialize the module with a given key **/
    @ReactMethod
    public void listenForAuth() {
        if (!initialized) {
            sendResult(false, NOT_INITIALIZED, "Module not initialized with .init()");
        }

        cancellationSignal = new CancellationSignal();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerprintManager.AuthenticationCallback authenticationCallback = new FingerprintManager.AuthenticationCallback() {
                @Override
                public void onAuthenticationError(int errorCode, CharSequence errString) {
                    sendResult(false, AUTHENTICATION_ERROR, "" + errorCode + " : " + errString);
                }

                @Override
                public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                    sendResult(false, AUTHENTICATION_HELP, "" + helpCode + " : " + helpString);
                }

                @Override
                public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
                    sendResult(true, null, null);
                }

                @Override
                public void onAuthenticationFailed() {
                    sendResult(false, AUTHENTICATION_FAILED, "Fingerprint did not match");
                }
            };
            fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, authenticationCallback, null);
        } else {
            sendResult(false, ANDROID_VERSION_TOO_LOW, "Android version M or higher is required.");
        }
    }

    @ReactMethod
    public void stopListeningForAuth(Promise promise) {
        if (cancellationSignal != null) cancellationSignal.cancel();
        promise.resolve(null);
    }

    public WritableMap makeResult(boolean authenticated, String errorCode, String errorMessage) {
        WritableMap result = new WritableNativeMap();
        result.putBoolean("authenticated", authenticated);
        result.putString("errorCode", errorCode);
        result.putString("errorMessage", errorMessage);
        return result;
    }


    protected void generateKey(String keyName) {
        try{
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator = KeyGenerator.getInstance(
                        KeyProperties.KEY_ALGORITHM_AES,
                        "AndroidKeyStore");
            }
        } catch(NoSuchAlgorithmException | NoSuchProviderException e ) {
            throw new RuntimeException("Failed to get KeyGenerator instance",e);
        }


        try{
            keyStore.load(null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator.init(new KeyGenParameterSpec.Builder(keyName, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());
            }
            keyGenerator.generateKey();
        } catch(NoSuchAlgorithmException|InvalidAlgorithmParameterException |CertificateException | IOException e){
            throw new RuntimeException(e);
        }

    }

    private void cipherInit(String keyName){
        try{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cipher = Cipher.getInstance(
                        KeyProperties.KEY_ALGORITHM_AES +"/"
                                + KeyProperties.BLOCK_MODE_CBC + "/"
                                + KeyProperties.ENCRYPTION_PADDING_PKCS7);
            }
        }catch (NoSuchAlgorithmException | NoSuchPaddingException e){
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try{

            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(keyName,null);
            cipher.init(Cipher.ENCRYPT_MODE,key);
        }catch(KeyStoreException | CertificateException | UnrecoverableKeyException | IOException |NoSuchAlgorithmException| InvalidKeyException e){
            throw new RuntimeException("Failed to init cipher",e);
        }
    }
}
